# github-platform-samples-python

[![CircleCI](https://circleci.com/bb/scantist-test-1/github-platform-samples-python/tree/master.svg?style=svg)](https://circleci.com/bb/scantist-test-1/github-platform-samples-python/tree/master)
[![Updates](https://pyup.io/repos/github/sfdye/github-platform-samples-python/shield.svg)](https://pyup.io/repos/github/sfdye/github-platform-samples-python/)
[![Python 3](https://pyup.io/repos/github/sfdye/github-platform-samples-python/python-3-shield.svg)](https://pyup.io/repos/github/sfdye/github-platform-samples-python/)

## How to use

```bash
# Install ngrok
$ brew install ngrok

# Listening to port 5000
./ngrok http 5000

# Install python dependencies
pip install -r requirements.txt

# Start Flask server
python webhook-example.py
```
